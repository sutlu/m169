# Woche 7 Nacharbeiten

## Dockerfile erstellen

Mit einem Dockerfile kann man Images erstellen. Es beinhaltet eine Reihe von Schritten. Anhand dieser Schritte wird das Image dann erstellt. 

Hier ein [beispiel](/Dockerfiles/nginx/Dockerfile) eines Dockerfiles. Dieses erstellt ein Image welches auf nginx:latest basiert und legt eine File im Document Root des Webservers an. Danach wird nginx gestartet und die Webseite ist nach start des Containers unter dem freigegebenen Port erreichbar.