# Projekt m169 Gitea

[TOC]

## Idee
Meine Idee ist mit Gitea einen eigenen Versionskontroll Server unter Docke am laufen zu haben. Dies will realisieren, indem ich eine eigenes Docker Image für Gitea erstelle, und danach mit Docker-Compose einen weiteren Container, in dem die benötigte SQL-Server Instanz läuft.

## Vorbereitung

Zum erstellen eines Dockerfiles muss ich zuerst verstehen wie Gitea ohne Docker aufgesetzt wir. Ich habe Gitea auf einer normalen Ubuntu Server VM aufgesetzt um die Anforderungen von Gitea an die Laufende Umgebung herauszufinden. Dies war relativ einfach mithilfe der [Gitea](https://docs.gitea.com/) Installationsanleitung. 
So habe ich gelernt welche User und Verzeichnissen mit welchen Eigenschaften angelegt werden müssen, und welche Packete installiert werden müssen.
Danach ging es auch schon an die Umsetzung.

## Umsetzung

### Dockerfile Gitea
Ich habe im [Dockerfile](Dockerfile) angefangen mit dem base image, bei welchem ich mich für ubuntu entschieden habe, da ich die Testinstallation unter Ubuntu getestet habe. Danach habe ich mit folgenden Befehlen zuerst alle Pakete akutallisiert und dann die benötigten heruntergeladen:
```Shell
RUN apt update && apt upgrade -y 
RUN apt install wget git sed -y
```

```Shell
RUN wget -O gitea https://dl.gitea.com/gitea/1.21.10/gitea-1.21.10-linux-amd64
RUN chmod +x gitea
RUN export GITEA_WORK_DIR=/var/lib/gitea/
RUN cp gitea /usr/local/bin/gitea
```

Weiter habe ich dann einen Benutzer erstellt:
```Schell
RUN adduser \
    --system \
    --shell /bin/bash \
    --gecos 'Git Version Control' \
    --group \
    --disabled-password \
    --home /home/git \
    git
```

Und die Verlangte Verzeichnissstrucktur gebaut:
```Schell
RUN mkdir -p /var/lib/gitea/custom && \
    mkdir -p /var/lib/gitea/data && \
    mkdir -p /var/lib/gitea/log && \
    chown -R git:git /var/lib/gitea && \
    chmod -R 750 /var/lib/gitea && \
    mkdir /etc/gitea && \
    chown root:git /etc/gitea && \
    chmod 770 /etc/gitea
```

Weiter, eine Umgebungsvariable für Gitea definiert:
```Shell
GITEA_WORK_DIR=/var/lib/gitea/
```
Und zum Schluss der Benutzer definiert, der über den Entrypoint beim starten des Containers Gitea startet:
```Shell
USER git
```
```Shell
ENTRYPOINT ["gitea", "web", "-c", "/etc/gitea/app.ini"]
```

Dies ist das Minimum, was gemacht werden muss, dass ein Gitea Container läuft.

### Docker-Compose
Gitea braucht aber noch eine Verbindung zu einer Datenbank, welche im Setup, im Webgui von Gitea eingebunden wird. Gitea kann auf einen beliebigen MySQL-Server zugreiffen. Ich habe das mit einem [Docker-Compose](docker-compose.yml) File gelöst, welche einen MySQL Container und einen Gitea Container startet.

Der erste Container, den ich im Docker-Compose file starte ist die Datenbank. Dies ist ein Standart MySQL image welches ich mithilfe von Environment Variabeln und einem init-db.sql file richtig konfiguriere:

```Shell
services:
  db:
    image: mysql:8.4
    environment:
      MYSQL_ROOT_PASSWORD: my-secret-pw  
      MYSQL_DATABASE: gitea  
      MYSQL_USER: gitea  
      MYSQL_PASSWORD: gitea-password
      # Alle oben angegebenen Passwörter sollten in einer Produktiven Umgebung wesentlich sicherer gestatlet werden
      # und am besten Verschlüsselt gespeichert werden.
    ports:
      - "3306:3306"
    volumes:
      - mysql_data:/var/lib/mysql
      - ./init-db.sql:/docker-entrypoint-initdb.d/init-db.sql
    restart: on-failure:10
    networks:
      gitea_net:
        ipv4_address: 10.20.0.20
```

Der Zweite Container ist Gitea von meinem eigenen Image gebaut. Ich binde da zusätzlich noch Verzeichnis von meinem Hostsystem ein, welche eine .ini datein für Gitea enthält. Später mehr dazu. Ebenfalls habe ich definiert, dass der Gitea Container vom db Container abhängig ist:

```Bash
  gitea:
    depends_on:
     - db
    image: gitea:1.0
    ports:
      - "3000:3000"
    restart: on-failure:10
    volumes:
      - /Users/lucasuter/Development/git/tbz/m169/projekt/src:/etc/gitea
    networks:
      gitea_net:
        ipv4_address: 10.20.0.10
```

Weiter habe ich noch ein Netzwerk für die Container definiert. Zu dem ebenfalls mehr im Zusätze abschnitt:

```Shell
networks:
  gitea_net:
    ipam:
      config:
        - subnet: 10.20.0.0/16
          gateway: 10.20.0.1
```

Und so laufen beide Container. Da ich das app.ini file bereits in den Container einbinde wird in meinem Fall auch kein Setup benötigt, sondern man kann direkt loslegen mit der Registration.

![Gitea läuft im Dockercontainer](../img/gitearunning.png)

## Zusätze

### Image Veröffentlichen

Ich habe mein Gitea Docker Image auf der Docker Registry veröffentlicht. Dafür muss auf dem Docker Hub ein Repository angelegt werden und dann mit folgenden zwei Commands das Image gebaut und Veröffentlicht werden.

Image Bauen:
```Shell
docker build . -t lucasuter103/lsgitea:0.1
```
Image in die Registry pushen:
```Shell
docker push lucasuter103/lsgitea:0.1
```

Sommit kann dieses Image von Jedem der Docker verwendet, gebraucht werden. Valentin Binotto hat mein Image auch getestet für seine Tests mit AWS ECS.

### Statische IP-Zuweisung
Ich vergebe in der Docker-Compose file den Containern fixe IP-Adressen und baue auch ein neues Netz für meine Docker-Umgebung. Dies habe ich nur getan, da ich leider nicht mehr dazu gekommen bin, das Gitea Dockerfile so zu erweitern, dass ich IP-Adressen Dynamisch dem Container übergeben werden können. Dies wäre benötigt, da die Gitea Instanz eine MySQL-Serveradresse braucht, diese aber erst während dem Setup im Web-GUI eingegeben werden muss. Also hatte ich zwei Möglichkeiten. Erstens hätte ich mit dem Command ```docker network inspect projekt_gitea_net ``` die von Docker automatisch Vergebenen IP-Adressen auslesen können, oder Zweitens, so wie ich es jetzt realisiert habe, im Docker Compose file, den Containern Fixe IP-Adressen vergeben können. In meinen Augen ist dies nicht unbedingt eine schlechte Anwendung. Da man bei Docker aber Stark auf Skalierbarkeit setzt kann diese Anwendung nur mit zusätzlichen Schritten einfach skaliert werden. Weiteres dazu unter dem Punkt Mögliche Erweiterungen.

## Mögliche Erweiterungen

### Umsetzung mit Environment Variablen
Das wohl Offensichtlichste, was ich in diesem Projekt weggelassen habe sind Environment Variablen. Die app.ini file wird von Gitea erst nach dem Setup im Web-GUI erzeugt. Sommit muss der Container bereits laufen, um Änderungen an der Configuration vornehmen zu können. Ich hätte dies lösen können, indem ich ein Bash-Skript schreibe, welches dieses app.ini File erzeugt, und die mitgegebenen Environment Variablen einfügt. Dazu hat leider die Zeit nichtmehr gereicht.

### Reverse Proxy (Sicherheit, Load-Balancing)
So wie meine Container jetzt laufen ist die Skalierbarkeit und vor allem Load-Balancing nur mit Aufwand möglich. Man müsste weitere Instanzen manuell mit anderen IP-Adressen versehen, dass mehrere dieser Container laufen könnten. Wenn man aber im Docker Compose File noch einen Reverse Proxy Container integrieren würde könnte dieser mit der von Docker integrierten Namensauflösung die Gitea Container ansteuern und somit auch Load-Balancen. Ein weiteres Problem ist damit aber nicht gelöst, und zwar, dass die Datenbanken der Gitea Instanzen synchronisiert werden müssten. Und das Problem beim Setup der nicht bekannten IP's wäre nur in kombination mit der einbindung von Umgebungsvariablen gelöst. Ein weiterer Vorteil eines Reverse Proxys wäre noch höhere Sicherheit.

## Reflexion
Dieses kleine Projekt hat viel Spass gemacht und ich habe viel über Docker und Docker-Compose gelernt. Rückblickend wäre es sinnvoller gewesen eine etwas simplere Anwendung als Gitea zu "Dockerisieren", da die Zeit schlichtweg nicht gereicht hat so tief wie es nötig gewesen wäre in diese Anwendung einzutauchen. Ein einfacheres Projekt hätte z.B. ermöglicht beim Bau des Dockerimages mit Umgebungsvariablen zu arbeiten. Trotzdem war das Projekt sehr lehrreich und hat mir aufgezeigt, wie einfach ein Deployment aussehen kann, wenn gute IaC im Sinne von Docker Compose und sinnvoll gebaute Images vorliegen.