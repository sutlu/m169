# Woche 5

[TOC]

## Container

### Eigenschaften von Containern in eigenen Worten beschreiben

Ein Container ist eine standardisierte Einheit von Software, die Code und alle seine Abhängigkeiten in einer isolierten Umgebung verpackt. Dies ermöglicht die Bereitstellung und Skalierung von Anwendungen über verschiedene Umgebungen hinweg. Container bieten eine konsistente Ausführung von Anwendungen unabhängig von der zugrundeliegenden Infrastruktur. Sie teilen sich die Ressourcen mit dem Host-Betriebsystem und brauchen wenig bis gar kein Overhead. Ebenfalls sind sie sehr lightweight, das heisst das keine Leistungsstarke Hardware zur Verfügung stehen muss, um mehrere gleichzeitig zu betreiben.

#### Docker

Nachfolgen einige wichtige Grundbegriffe im zusammenhang mit Docker:

1. **Docker Deamon**: verwaltet und überwacht Container, orchestriert ihre Erstellung, Ausführung und Beendigung sowie deren Interaktion mit der zugrunde liegenden Infrastruktur. Er dient zum Bauen und Speichern von Images.
2. **Docker Client**: Docker wird über CLI mittels des Docker Clients bedient. Er Kommuniziert über HTTP REST mit dem Daemon.
3. **Images**: ist eine eigenständige, ausführbare Softwareeinheit, die Code, Laufzeitumgebung, Bibliotheken und Abhängigkeiten enthält und als Vorlage für die Erstellung von Containern dient. Sie bestehen aus Name und Version z.B. Ubuntu:16.04 (wird keine Version angegeben wird automatisch :latests gewählt)
4. **Docker Registry**: Darauf werden Images abgelegt und verteilt. Die Standart Registry ist der Docker Hub.

### Bestehende Container einsetzen.

Mit folgendem Befehl starte ich eine simplen Docker Container, mit einer Interaktiven Shell:
```bash
docker run -it ubuntu /bin/bash
```
Sobald der Container läuft sind wir direkt in der bash (da das -d weggelassen wurde) und können gängige befehle von Linux ausführen.

![docker run ubuntu bash](img/dockerrunbash.png)

Dieser Befehl nimmt ein bestehendes Ubuntu docker Image und startet dieses.

Diese Befehle sind vielfältig erweiterbar und man kann anhand vom Docker Hub bereitgestellten Images schnell auch komplexe container starten.

Ein etwas komplexerer kann so aussehen:
```bash
docker run --name vaultwarden -v /Users/lucasuter/vw-data/:/data/ \
        -e LOG_FILE=/data/vaultwarden.log \
        -e TZ=Europe/Paris \
        -p 127.0.0.1:8080:80 vaultwarden/server:latest
```

Dieser Befehl startet einen Container mit dem namen vaultwarden, erstellt eine Datenablage im Container unter /data welche vom Host-System unter /vw-data zugreifbar sind. Weiter geben wir zwei Environment-Variablen mit einemal wo das Log-File hingeschrieben werden soll und in welcher Zeitzone wir uns befinden. Zum schluss sagen wir, dass sich Port 8080 vom Localhost vom Hostsysten auf den Port 80 im Container weitergeleitet werden soll.

Nun steht mit einem Einzeiler in der bash ein Dockerconteiner welcher einen Vollumfänglichen Passwortmanager hostet. Dieser ist auch bereits unter unter localhost:8080 erreichbar.

![docker vaultwarden cmd](img/vaultwardencmd.png)
![docker vaultwarden web](img/vaultwardenweb.png)

Um diesen Passwortmanager effektiv nutzen zu können bräuchte man noch weitere Komponenten. Es ist beispielsweise am einfachsten, wenn man einfach einen Reverseproxy laufenlässt, den den einkommenden Verkehr auf den Dockercontainer weiterleitet. So kann man eine die Vaultwarden Webseite einfacher selbst zertifiziert aus dem Web aufrufen (wird von Vaultwarden für einige Features benötigt). Und natürlich würde es auch Sinn ergeben, das ganze (mit den zutreffenden Sicherhetisvorkehrungen) ins Internet zu stellen. Eine Docker compose file für den Vaultwarden Container und ein Caddy (reverse proxy) Container aussehen:
```yml
version: '3'

services:
  vaultwarden:
    image: vaultwarden/server:latest
    container_name: vaultwarden
    restart: always
    environment:
      DOMAIN: "https://vaultwarden.example.com"  # Your domain; 
      # vaultwarden needs to know it's https to work properly with attachments
    volumes:
      - ./vw-data:/data

  caddy:
    image: caddy:2
    container_name: caddy
    restart: always
    ports:
      - 80:80  # Needed for the ACME HTTP-01 challenge.
      - 443:443
    volumes:
      - ./Caddyfile:/etc/caddy/Caddyfile:ro
      - ./caddy-config:/config
      - ./caddy-data:/data
    environment:
      DOMAIN: "https://vaultwarden.example.com"  # Your domain.
      EMAIL: "admin@example.com"                 # The email address to use for ACME registration.
      LOG_FILE: "/data/access.log"
``` 

Ebenfalls ist hier ersichtlich das bei der Caddy config direkt config Files mitgegeben werden. Auf dem Host-System muss ebenfalls einen FQDN über DNS vergeben sein und etwas wie Let's encrypt für ein gültiges Zertifikat muss eingerichtet sein.

## Netzwerk-Anbindung

Weiterleitung von Ports zwischen Host-System und Container:

Wir starten mit Folgendem Command ein mysql Container:

```bash
docker run --name mymysqlserver -e "MYSQL_ROOT_PASSWORD=abcd1234" -it -d -p 3307:3306 mysql
```

Wenn wir den Teil bei -p weiter betrachten sehen wird, dass wir den Port 3307 (es lief zur zeit auf meinem Laptop eine MariaDB instanz) vom Host System auf den Port 3306 im Container weiterleiten. 

Nun kann ich also auf dem Host-System auf den mySql Server über den Port 3307 zugreifen:

![mysql server Zugriff](img/mysqlconteinerext.png)

Dies kann wie auch oben schon beschrieben auch weiter geführt werden, dass der Service aus dem LAN oder Internet erreichbar gemacht werden kann.

Weitere Konfigurationen von Netzwekren sind möglich mit Docker einen Teil davon habe ich im [Projekt](projekt/projekt.md) abgebildet.

## Distributed Services und asynchrone Services

Docker eignet sich sehr gut, um verteilte und asynchrone Services zu verwalten. Wie wir gelernt haben dient Docker dazu Anwendungen zu Containerisieren, was die Definition und Verwaltung der Schnittstellen zwischen services erleichtert. Dies bedeutet konkret, dass es einfach ist, z.B. einem Container eine IP-Adresse oder weitere Parameter, über Umgebungsvariablen mitzuteilen. Oder das bei einem Container nur spezifische Ports geöffnet sind. Wenn diese Schnittstellen z.B. so definiert erden, dass die Container über einen Load-Balancer/Reverse-Proxy angesteuert werden, was zu einer höheren Systemeffizienz und besserer Fehlerresilienz führt.
Weiter Vereinfacht Docker die Entwicklung und das Deployment von verteilten Systemen, da in Docker optimalerweise die Services Isoliert von einander Laufen. Diese Isolation unterstützt die asynchrone Kommunikation, da jeder Service unabhängig aktualisiert und skaliert werden kann, ohne andere Teile des Systems zu beeinträchtigen.

**Port binden**
```Shell
docker run -p "3307:3306" mysql:8.4
```
Mit der -p Flag wird definiert welcher Port vom Hostsystem (erster Port) and den Container (zweiter Port) weitergeleitet wird. So kann von Ausserhalb des Dockernetzwerks nur über den Port 3307 mit dem Container Kommuniziert werden.