# Woche 6 Container Sicherheit

[TOC]

## Protokollieren und Überwachen

### Logging

**Standart-Logging:**

Mit folgenden Befehlen können wir das Standart-Logging veranschaulichen:
```bash
docker run --name logtest ubuntu bash -c 'echo "stdout"; echo "stderr" >>2'
docker logs logtest
```

Oder wenn wir beispielsweise einen mySql Server starten wollen, aber das Root Passwort für die Datenbank nicht gesetzt ist. ist das ebenfalls mit dem ``` docker logs ``` nachvollziehbar, was der Container von uns will. 

![docker logs](img/dockerlogs.png)

Weiter kann so beispielsweise nachvollziehbar gemacht werden, weil z.B. ein Service im Container abgestürzt ist oder im Allgemeinen, wieso der Container nicht das macht, was man vielleicht erwartet.

**Syslog**

Mit der flag ```--log-driver=syslog``` kann man beim Start eines Containers auch definieren, dass der ganze Docker Log in den Syslog des Hostsystems geschrieben werden kann:

![docker syslog](img/dockerdriversyslog.png)

### Überwachen und Benachrichtigen

Um die Ressourcen von einem Server, auf dem diverse Container laufen, zu überwachen gibt es ein Tool von Google namens cAdvisor. Dieses stellt auf einer im Container gehostete Webseite Graphisch der Ressourcenverbrauch für den Server aber auch für die einzelnen Container dar.

Docker Befehl für Cadvisor Container:
```Shell
docker run -d --name cadvisor -v /:/rootfs:ro -v /var/run:/var/run:rw -v /sys:/sys:ro -v /var/lib/docker/:/var/lib/docker:ro -p 8080:8080 gcr.io/cadvisor/cadvisor
```

![Cadvisor](img/Cadvisor.png)

Ressourcen:

![Cadvisor ressourcen](img/cadvisorrecources.png)

Spezifischer Container:

![Cadvisor Container](img/cadvisorcontainers.png)

Falls ein Alerting benötigt wird muss dies selbst gebaut werden oder andere Tools dafür einsetzen. cAdvisor dient lediglich der Überwachung. Dies kann mit Prometheus gemacht werden. 

## Container sichern & beschränken

**Netzwerkzugriff beschränken**

Bei Produktiven Containern sollten nur die effektiv benötigten Ports geöffnet werden. Das heisst auch, dass andere Container nur dann auf einen Container zugreifen können sollten, wenn sie wirklich müssen.

**setuid/setgid-Binaries entfernen**

Unter Unix wird mit setuid/setgid festgelegt, falls User die keine root Rechte haben ein Programm, dass eigentlich root rechte zur Ausführung braucht, ausführen wollen, das trotzdem dürfen. Ein Beispiel dafür ist ```passwd```. 

**Speicher, CPU, Neustarts Begrenzen**

Es ist sinnvoll, die Ressourcen eines Containers zu begrenzen, dass bei Angriffen, die zu hohen CPU oder RAM Auslastung führen genügend Ressourcen für andere laufende Container übrig bleibt. Dies macht man mit flags in einem docker run Befehl mit beispielsweise: -c 2048, -m 128m, --restart=on-failure:10. Die -c Flag sagt Docker wie viel CPU-Time ein Container relativ brauchen darf. Wenn nichts angegeben wird ist der standart Wert 1024. Das heisst wenn man eine höhere Zahl mitgibt erhält der Container mehr CPU-Zeit, dementsprechend bei einer kleineren Zahl weniger. Mit -m kann man den Arbeitsspeicher begrenzen. Und mit --restart=on-failure:10 wird definiert, dass der Container bei einem Crash nur 10 mal versucht neu zu starten und nicht wie wenn man always mit gibt versucht neu zu starten, bis er manuell gestoppt wird.

