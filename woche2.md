# Woche 2

[TOC]

## Infrastruktur as Code (IaC)

### Was ist IaC:

IaC ermöglicht die Verwaltung und Bereitstellung von Infrastruktur durch Code anstelle manueller Prozesse. Konfigurationsdateien enthalten die gesamten Infrastrukturspezifikationen, erleichtern die Bearbeitung und Verteilung von Konfigurationen und gewährleisten konsistente Umgebungen. Versionskontrolle ist entscheidend, um Konfigurationsdateien zu verfolgen und Änderungen zu dokumentieren. IaC automatisiert die Infrastrukturprovisionierung und ermöglicht modulare Komponenten, die flexibel kombiniert werden können.

### Deklarativer und imperativer IaC-Ansatz im Vergleich:

Es gibt zwei IaC-Ansätze: deklarativ und imperativ. Der deklarative Ansatz definiert den gewünschten Zustand des Systems, während das IaC-Tool die Konfiguration automatisch vornimmt. Der Zustand der Systemobjekte wird verfolgt, was das Herunterfahren der Infrastruktur vereinfacht. Im Gegensatz dazu werden beim imperativen Ansatz konkrete Befehle definiert, die in der richtigen Reihenfolge ausgeführt werden müssen. Viele IaC-Tools verwenden einen deklarativen Ansatz, der automatische Anpassungen bei Änderungen am gewünschten Zustand ermöglicht. Einige Tools unterstützen beide Ansätze, wobei die Wahl vom Anwendungsfall abhängt.

### Vorteile von IaC:

Die Einführung von IaC bietet zahlreiche Vorteile, besonders angesichts der komplexen und sich ständig ändernden Anforderungen moderner IT-Infrastrukturen. Zu den wichtigsten Vorteilen gehören:

1. Geringere Kosten: IaC ermöglicht eine effizientere Ressourcennutzung, automatisierte Prozesse reduzieren den Bedarf an manueller Arbeit und minimieren somit Kosten.
2. Schnellere Deployments: Die automatisierte Bereitstellung von Infrastruktur beschleunigt den Bereitstellungsprozess von Anwendungen erheblich, was zu schnelleren Markteinführungszeiten führt.
3. Weniger Fehler: Die Vermeidung manueller Konfigurationen reduziert menschliche Fehler und verbessert die Gesamtqualität der Infrastruktur.
4. Bessere Infrastrukturkonsistenz: IaC gewährleistet, dass die Infrastruktur in verschiedenen Umgebungen konsistent ist, was zu weniger Kompatibilitätsproblemen führt.
5. Keine Konfigurationsabweichungen: Da die Infrastruktur als Code definiert ist, werden Abweichungen von den gewünschten Konfigurationen vermieden.

### IaC Tools:

Für die Implementierung von IaC stehen verschiedene Tools zur Verfügung. Einige der beliebten Produkte sind:

- Chef: Ein Konfigurationsmanagement-Tool, das den Zustand von Systemen deklarativ beschreibt
- Puppet: Ein weiteres Konfigurationsmanagement-Tool, das eine deklarative Syntax verwendet, um Systeme zu verwalten.
- Red Hat Ansible Automation Platform: Ein deklaratives IaC-Tool, das die Automatisierung von Betriebssystemen, Netzwerkgeräten und Anwendungen ermöglicht.
- Saltstack: Ein IaC-Tool, das sich auf die Remote-Ausführung von Befehlen und Konfigurationsmanagement konzentriert.
- Terraform: Ein Tool für die Bereitstellung und das Management von Infrastruktur als Code, das sich auf die Orchestrierung von Ressourcen konzentriert.
- AWS CloudFormation: Ein Amazon Web Services-Tool, das die Bereitstellung und Verwaltung von AWS-Ressourcen durch Templates ermöglicht.


[Quelle: redhat.com](https://www.redhat.com/de/topics/automation/what-is-infrastructure-as-code-iac)

## Beispiel für IaC

Ich zeige hier ein paar Beispiel in Ansible.

Nehmen wir an wir haben einen rocky Linux 9 Server und  wollen nun darauf ein Dockercontainer, in welchem eine Vaultwarden (von Bitwarden geforkter Passwortmanager) laufen lassen:

```
- name: install docker package
  package:
    state: present
    name: docker-ce

- name: pull vaultwarden docker image
  docker_image:
    name: vaultwarden/server:latest
    source: pull

- name: prepare vaultwarden shell script
  template: 
    src: vaultwarden.sh.j2
    dest: "/usr/local/bin/VaultWarden"

- name: prepare systemd service
  template:
    src: vaultwarden.service.j2
    dest: "{{os_systemd_service_path}}/vaultwarden.service"
    mode: '654'
  notify: reload systemd

- meta: flush_handlers

- name: enable and start vaultwarden service
  service:
    name: vaultwarden
    state: started
    enabled: true
  ignore_errors: "{{ansible_check_mode}}"
```

Dieses Skript schaut also, dass Docker installiert ist, zieht sich das vorgefertigte Dockerimage für die Vaultwarden Instanz. Erstellt anhand eines Templates eine Systemd unit und ein shellscript, welches die Dockerinstanz startet und startet den Service.

Natürlich macht es auch Sinn, wenn ein neuer Server/virtueller Server aufgesetzt wird so viel wie möglich über Ansible zu konfigurieren (ansonsten würden wir ja nur Bruchteile von IaC nutzen). Da in Linux viel anhand von einfachen Files konfiguriert wird ist z.B. eine Netzwerkkonfiguration von einem Linux Server relativ einfach. Man erstellt mit z.B. Jinja2 ein dynamisches Template für ein Tool wie Network-Manager und kann dann mit Variablen statische IP-Adressen, Hostnamen und  DNS-Konfigurationen angeben.

## Cloud-Services

Cloud-Services haben wir bereits im Modul 346 an der TBZ angeschaut. Hier ein Link zu meinem Git-Repository zu diesem Thema: [Git-Repo-m346](https://gitlab.com/sutlu/m346). 
Darin haben wir gelernt, wie man mit Cloud-Init Dateien Virtuelle Maschinen schnell und einfach konfigurieren kann. Weiter haben wir gelernt, wie man Load-Balancer, automatische Skalierung, uvm. verwirklicht.

## Sicherheit einer Virtuellen Infrastruktur verbessern

### 1. Firewall und Reverse Proxy

#### Firewall

Eine Firewall ist ein Sicherheitssystem, das den Netzwerkverkehr zwischen einem Computer oder Netzwerk und dem Internet überwacht und regelt. Sie ermöglicht oder blockiert den Datenverkehr basierend auf vordefinierten Sicherheitsregeln, um unerwünschte Zugriffe und potenzielle Bedrohungen abzuwehren.

Ein beispiel dafür unter Linux ist ufw. Dies ist ein einfach zu bedienendes CLI Tool, welches auf iptables basiert.

Beispielbefehle für ufw:

UFW aktivieren:
```sudo ufw enable```

Port öffnen (z.B., Port 80 für HTTP):
```sudo ufw allow 80```

Zugriff von einer bestimmten IP zulassen:
```sudo ufw allow from 192.168.1.2```

Port für ein bestimmtes Protokoll öffnen (z.B., TCP):
```sudo ufw allow 22/tcp```

Port für ein bestimmtes Protokoll und eine bestimmte IP öffnen:
```sudo ufw allow from 192.168.1.2 to any port 22 proto tcp```

Dienste nach Namen zulassen (z.B., OpenSSH):
```sudo ufw allow OpenSSH```

Zugriff auf alle Ports von einer bestimmten IP verweigern:
```sudo ufw deny from 192.168.1.2```

Denke daran, nach Änderungen die Firewall neu zu laden:
```sudo ufw reload```

#### Reverse Proxy

Ein Reverse Proxy ist ein Server, der als Vermittler zwischen Clients und einem oder mehreren Backend-Servern agiert. Hier sind die grundlegenden Schritte, wie ein Reverse Proxy funktioniert:

1. **Client-Anfrage:**
   Ein Client sendet eine Anfrage an den Reverse Proxy. Diese Anfrage kann beispielsweise eine Anforderung für eine Webseite, einen bestimmten Dienst oder eine Ressource sein.

2. **Weiterleitung der Anfrage:**
   Der Reverse Proxy empfängt die Anfrage und entscheidet, welcher Backend-Server die Anfrage bedienen soll. Diese Entscheidung kann auf verschiedenen Kriterien basieren, wie Lastenausgleich, Standort, Art der Anfrage, etc.

3. **Kommunikation mit Backend-Servern:**
   Der Reverse Proxy leitet die Anfrage an den ausgewählten Backend-Server weiter. Dies kann eine einzelne Anfrage oder eine Serie von Anfragen im Verlauf einer Sitzung sein.

4. **Verarbeitung durch Backend:**
   Der ausgewählte Backend-Server bearbeitet die Anfrage und sendet die entsprechende Antwort zurück an den Reverse Proxy.

5. **Weiterleitung der Antwort an den Client:**
   Der Reverse Proxy nimmt die Antwort des Backend-Servers und leitet sie an den ursprünglichen Client weiter.

Ein wesentlicher Vorteil eines Reverse Proxys liegt in der Fähigkeit, mehrere Backend-Server zu koordinieren, Load-Balancing zu betreiben, Sicherheitsfunktionen bereitzustellen (wie SSL-Terminierung) und die Clients von den Details der Backend-Server zu isolieren. Dadurch ermöglicht er eine flexiblere und skalierbare Architektur für Webanwendungen und Dienste.

### 2 Benutzer- & Rechteverwaltung

Die Benutzer- und Rechteverwaltung bezieht sich auf die Kontrolle und Verwaltung des Zugriffs auf Ressourcen in einem Computersystem. Hier sind die grundlegenden Konzepte:

**Benutzer:**
Ein Benutzer ist eine identifizierte Person oder ein System, das auf ein Computersystem zugreift. Jeder Benutzer hat ein eindeutiges Benutzerkonto, das durch einen Benutzernamen und oft auch durch ein Passwort identifiziert wird.

**Rechte:**
Rechte definieren, welche Aktionen oder Ressourcen ein Benutzer innerhalb des Systems ausführen kann. Dies kann den Zugriff auf Dateien, Verzeichnisse, Programme, Netzwerkressourcen und andere Systemfunktionen umfassen.

**Benutzer- und Gruppenzuweisung:**
Benutzer werden in Gruppen organisiert, und Rechte können Benutzern oder Gruppen zugeordnet werden. Dies ermöglicht eine effiziente Verwaltung von Berechtigungen, indem sie auf Gruppenebene definiert und dann Benutzern zugewiesen werden.

Insgesamt ermöglicht die Benutzer- und Rechteverwaltung die effektive Organisation und Steuerung des Zugriffs auf Informationen und Systemfunktionen, wodurch die Sicherheit und Integrität von Computernetzwerken gewährleistet wird.

### 3 SSH

SSH steht für "Secure Shell" und ist ein Protokoll, das sichere Netzwerkkommunikation ermöglicht. Es wird häufig für die sichere Verwaltung von entfernten Systemen verwendet. Hier sind einige Schlüsselaspekte von SSH:

**Sichere Datenübertragung:**
SSH bietet eine verschlüsselte Verbindung zwischen einem Client und einem Server. Dies schützt vor Abhören und Manipulation der übertragenen Daten, was besonders wichtig ist, wenn vertrauliche Informationen über unsichere Netzwerke übermittelt werden.

**Authentifizierung:**
SSH ermöglicht die sichere Überprüfung der Identität eines Benutzers oder eines Systems, bevor der Zugriff gewährt wird. Dies erfolgt normalerweise durch die Verwendung von Benutzername und Passwort, kann jedoch auch durch den Einsatz von Schlüsselpaaren für eine sicherere Authentifizierung erfolgen.

**Sichere Remote-Zugriff:**
SSH ermöglicht es Benutzern, sich von einem entfernten Standort aus mit einem Server zu verbinden und diesen zu verwalten, als wäre man direkt vor Ort. Dies ist besonders nützlich für die Fernwartung von Servern oder die Durchführung von Aufgaben auf entfernten Rechnern.

**Tunneling und Port Forwarding:**
SSH ermöglicht auch das Erstellen von verschlüsselten Tunneln für die sichere Übertragung anderer Netzwerkprotokolle. Dies wird oft für Port Forwarding und sichere Übertragung von Daten verwendet.

Insgesamt bietet SSH eine sichere Methode für die Fernverwaltung von Systemen und den sicheren Datenaustausch über unsichere Netzwerke. Es ist ein wichtiger Bestandteil der Netzwerksicherheit und wird in vielen verschiedenen Szenarien eingesetzt, von der Verwaltung von Servern bis zur sicheren Datenübertragung.

Wichtige Commands:

Auf ein entferntes System verbinden:
```ssh <benutzer>@<ip/hostname>```

Kopieren des eigenen Publickeys auf das Remotesystem:
```ssh-copy-ip <benutzer>@<ip/hostname>```

Befehl auf entferntem system ausführen:
```ssh <ip/hostname> <Befehl>```

Kopieren von Daten von einem System zu einem Anderen:
```scp <datei> <ip/hostname>:<datei>```
```scp <ip/hostname>:<datei> <datei>```

### 4 Authentifizierung & Autorisierung

Authentifizierung ist der Prozess des Nachweises einer behaupteten Eigenschaft einer Entität, wie z. B. eines Benutzers oder Geräts, wodurch diese als authentisch gilt. Dieser Prozess kann Berechtigungssprüfungen umfassen und ermöglicht der authentifizierten Entität weitere Aktionen.

Autorisierung ist die Zustimmung oder Einräumung von Rechten gegenüber Interessenten, wie beispielsweise dem Zugriff auf Ressourcen in einem Netzwerk oder der Installation von Software. Autorisierung erfolgt nach erfolgreicher Authentifizierung und gilt gegebenenfalls eingeschränkt in einem bestimmten Kontext oder Modus.

## Mikroservices Architektur

Microservices sind eine Architekturstil, bei dem eine Anwendung als Sammlung kleiner, unabhängiger Dienste entwickelt wird, die jeweils einen spezifischen Geschäftsbereich abdecken. Jeder Service läuft in einem eigenen Prozess und kommuniziert mit anderen Diensten über wohldefinierte Schnittstellen, typischerweise APIs.

### Vorteile
- Modularität: Einzelne Services können unabhängig von einander weiterentwickelt werden.
- Skalierbarkeit: Microservices können unabhängig voneinander skaliert werden, abhängig von den Anforderungen des jeweiligen Services.
- Fehlertoleranz: Bei einem Fehler in einem Service wird nicht die gesamte Anwendung beeinträchtigt. Nur der betroffene Service wird beeinflusst, was die Gesamtstabilität verbessert.

### Nachteile 
- Konplexität im Management:  Die Verwaltung zahlreicher kleiner Services kann komplex sein, insbesondere was die Service-Orchestrierung und die Überwachung betrifft.
- Datenkonsistenz: Jeder Microservice kann seine eigene Datenbank haben, was die Datenkonsistenz über das gesamte System hinweg erschweren kann.

### Fazit zu Mikroservice Architektur
Nicht alle Anwendungen brauchen zig verschiedene Services die laufen. Wie bei den Vorteilen, macht es Sinn Mikroservices zu verwenden, wenn verschiedene Bausteine für eine Anwendung benötigt werden. Wenn hingegen z.B. nur ein kleiner Webserver gebraucht wird ohne Datenbank usw., ist es in meinen Augen wesentlich simpler dies auf bare metal oder in einer Kleinen VM zu betreiben.