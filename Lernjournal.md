# Lernjournal

[TOC]

## Woche 1
### 1.1 Wochenziele
  - [x] Toolumgebung einrichten
    - [x] git Repository anlegen
    - [x] VSCode installieren
    - [x] VSCode Extensions installieren
### 1.2 Probleme
Probleme gab es bei der einrichtung von Git und VSCode keine. Ich nutze beide Tools fast täglich in der Schule oder bei der Arbeit.

### 1.3 Ressourcen

### 1.4 Praktische Umsetzung
Hier gibt es nicht viel zu sagen. Ich habe alle Tools bereits vor dem Start dieses Moduls installiert und eingesetzt gehabt. Ebenfalls habe ich eine Markdown-Preview Extension in VSCode installiert.
![code](/img/code.png)


## Woche 2
### 2.1 Wochenziele
  - [x] Ich verstehe die Nutzung von Infrastructure as Code und Cloud-Services um IT-Infrastruktur effizient zu automatisieren, was Skalierbarkeit und Konsistente Umgebungen in der Softwareentwicklung und Systemverwaltung ermöglicht.
  - [x] Ich kann die Sicherheit in einer Virtuellen Infrastruktur verbessern.

### 2.2 Probleme
Ich bin lediglich auf ein Problem gestossen: Ich hatte keine zeit alles praktisch umzusetzen. Dies versuche ich in der kommenden Woche oder während des Projekt zu realisieren.

### 2.3 Ressourcen
- Gitlab der TBZ
- [redhat.com](https://www.redhat.com/de/topics/automation/what-is-infrastructure-as-code-iac)
- chatGPT

### 2.4 Praktische Umsetzung

Ich habe eine Ansible Role geschrieben, die einen Vaultwarden Docker Container auf einem Linux Server erstellt und Startet. Im Modul 346 haben wir diverse Praktische Übungen zu Cloud-Services durchgeführt. Leider hatte ich wie oben beschrieben nicht genug zeit um alles Praktisch anzuwenden.


## Woche 3
### 3.1 Wochenziele
  Abwesend
### 3.2 Probleme

### 3.3 Ressourcen

### 3.4 Praktische Umsetzung

## Woche 4
### 4.1 Wochenziele
  Abwesend
### 4.2 Probleme

### 4.3 Ressourcen

### 4.4 Praktische Umsetzung

## Woche 5
### 5.1 Wochenziele
  - [x] Container: 
    - [x] Ich kann Eigenschaften von Containern in eigenen Worten beschreiben und bestehende Container einsetzen.
    - [x] Ich kann eine Container Umgebung mit einem oder mehreren Services erläutern.
  - [x] Docker
  - [x] Netzwerk-Anbindung
  - [ ] Volumes
### 5.2 Probleme

Vereinzelt kleine Probleme mit der Syntax (wenn man das so nennen darf) der Docker Commands. Z.B. beim einbinden von Umgebungsvariablen.

Leider bin ich erneut nicht so weit gekommen, wie ich wollte. Ich habe mich etwas in den Anwendungsbeispielen von Vaultwarden verloren, und mit mit der Nacharbeit der Letzten beiden Wochen etwas übernommen. Ich habe z.B. zu viel Ausprobiert und dabei zu wenig Dokumentiert. Die Zeit über die Osterferien Nutzen, um einiges Aufzuarbeiten.

### 5.3 Ressourcen  
 - GitLab der TBZ
 - [Vaultwarden official gitlab](https://github.com/dani-garcia/vaultwarden/wiki/Using-Docker-Compose)

### 5.4 Praktische Umsetzung

Siehe [Woche 5](woche5.md).

## Woche 6
### 6.1 Wochenziele
  - [ ] Nacharbeit Woche 5
  - [x] Container Sicherheit
  - [ ] Podman
### 6.2 Probleme
Probleme gab es bisher keine. In der Lektion hatte ich noch zu wenig Zeit für die Nacharbeit. Habe aber das Kapitel Containersicherheit fast ganz abgearbeitet.
### 6.3 Ressourcen
 - GitLab der TBZ
### 6.4 Praktische Umsetzung
Siehe [Woche 6](woche6.md)

## Woche 7
### 7.1 Wochenziele
  - [ ] Nacharbeit Dockerfile
  - [ ] Podman
### 7.2 Probleme
Auf der Dockerversion für MacOS gibt es einen Bug, bei welchem man keine Images builden kann. Dieser hat viel Zeit gekostet zu umgehen. Das Problem war, dass Docker keine Berechtigungen auf eine Datei im Filesystem hatte. Als ich die Berechtigungen angepasst habe hat es wieder funktioniert (Im Docker Desktop Programm funktioniert der Build Tab nach wie vor nicht, ich kann die Images aber natürlich in der CLI abrufen). Das Beheben dieses Fehlers hat leider viel Zeit gekostet.
### 7.3 Ressourcen
 - GitLab der TBZ
### 7.4 Praktische Umsetzung
Siehe [Woche 7](woche7.md)

## Woche 8
### 8.1 Wochenziele
  - [x] Nacharbeit Dockerfile
### 8.2 Probleme
Probleme gab es diese Woche keine, da ich die Ziele auch nicht sehr hoch gesteckt habe, dass ich diese auch einmal erreiche.
### 8.3 Ressourcen
- [YouTube Video](https://youtu.be/DESdVoKhIxY?si=BoJRLTQZr9Y6i29R)
### 8.4 Praktische Umsetzung
Ich habe diese Woche relativ wenig Dokumentiert. Aber im Ordner Dockerfiles sind zwei kleine Projekte. Einmal ein einfacher Nginx container und das zweite Projekt erstellt ein Image das ein Stück javascript im container ausführt und unter einer URL abrufbar ist.

## Woche 9
### 9.1 Wochenziele
  - [x] Projekt starten
### 9.2 Probleme
Probleme gab es bis jetzt keine nennenswerten. Ich habe eine gute Basis gelegt und ein Dockerfile erstellt. Dieses läuft aber noch nicht durch. Das zu lösen veruche ich nächste Woche.
### 9.3 Ressourcen
 - [Gitea](https://docs.gitea.com/)
### 9.4 Praktische Umsetzung
Siehe [Projekt](projekt/projekt.md)

## Woche 10
### 10.1 Wochenziele
 - [x] Projekt abschliessen
 - [x] Dokumentation Projekt
### 10.2 Probleme
Ich habe mir, mit der Idee ein eigenes Dockerfile für eine Gitea Instanz zu erstellen, selbst in den Fuss geschossen. Ich habe es fertig gebracht, dass Gitea aus meinem Dockerfile heraus läuft. Da ich aber den SQL Server in einem anderen Container laufen lasse, brauche ich Docker Compose um einen SQL Container zu starten. Weitere Probleme, auf die ich gestossen bin sind im [Projekt](projekt/projekt.md) erläutert und reflektiert.
### 10.3 Ressourcen
 - [Gitea](https://docs.gitea.com/)
 - GitLab der TBZ
### 10.4 Praktische Umsetzung
Siehe [Projekt](projekt/projekt.md)